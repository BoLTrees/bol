class 'OnConsumable' -- {
local buffNames = { 'RegenerationPotion', 'FlaskOfCrystalWater', 'PotionOfGiantStrength', 'PotionOfBrilliance', 'ItemMiniRegenPotion', 'ItemCrystalFlask', 'OracleExtractSight', 'ElixirOfRage', 'ElixirOfIllumination' }
local mode = false
function OnConsumable:__init()
	if _G.OnConsumableSimpleMode ~= nil then mode = _G.OnConsumableSimpleMode end
	if not mode then
		-- register full callbacks
		callBacks = { 
							{ 'OnGainHPPot', 'OnLoseHPPot', 'OnUpdateHPPot'}, 
							{ 'OnGainMPPot','OnLoseMPPot', 'OnUpdateMPPot'}, 
							{ 'OnGainRedPot', 'OnLoseRedPot', 'OnUpdateRedPot'}, 
							{'OnGainBluePot','OnLoseBluePot', 'OnUpdateBluePot'}, 
							{'OnGainBiscuit', 'OnLoseBiscuit', 'OnUpdateBiscuit'}, 
							{'OnGainFlask','OnLoseFlask', 'OnUpdateFlask'},
							{'OnGainOracles', 'OnLoseOracles', 'OnUpdateOracles'} 
						}
						
		for i,v in ipairs(callBacks) do
			_ENV = AdvancedCallback:register(table.unpack(callBacks[i]))
		end		
	else
		-- register basic callbacks
		callBacks = { 'OnHPPot', 'OnMPPot', 'OnRedPot', 'OnBluePot', 'OnBiscuit', 'OnFlask', 'OnOracles' }
		_ENV = AdvancedCallback:register(table.unpack(callBacks))
	end
	-- bind buff callbacks to buffHandler
	AdvancedCallback:bind('OnGainBuff', function(unit, buff) self:buffHandler(unit, buff, 1) return end)
	AdvancedCallback:bind('OnLoseBuff', function(unit, buff) self:buffHandler(unit, buff, 2) return end)
	AdvancedCallback:bind('OnUpdateBuff', function(unit, buff) self:buffHandler(unit, buff, 3) return end)
end

function OnConsumable:buffHandler(unit, buff, gain)
	if unit and unit.valid then
		--cycle through buffNames to determine the buff
		for i,v in ipairs(buffNames) do 
			if buff.name == v then 
				if not mode then 
					-- CallBack(unit, buff)
					AdvancedCallback[callBacks[self:switchI(i)][gain]](AdvancedCallback, unit, buff)
				else
					-- CallBack(unit, buff, gain)
					AdvancedCallback[callBacks[self:switchI(i)]](AdvancedCallback, unit, buff, gain)
				end
				break
			end
		end
	end
end

function OnConsumable:switchI(thisI)
	if thisI <= 7 then return thisI
	elseif thisI == 8 then return 3
	elseif thisI == 9 then return 4
	end
end
-- }