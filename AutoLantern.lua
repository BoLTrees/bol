local lant

function OnLoad()
	Menu = scriptConfig('AutoLantern', 'Lantern')
	Menu:addParam('Lantern', 'Use Lantern', SCRIPT_PARAM_ONKEYDOWN, false, 32)
	Menu:addParam('Auto', 'Auto-Lantern at Low HP', SCRIPT_PARAM_ONOFF, true)
	Menu:addParam('Health', 'Auto-Lantern Health %', SCRIPT_PARAM_SLICE, 20, 0, 100, 2)
	if myHero.charName == 'Blitzcrank' then Menu:addParam('Blitz', 'Lantern on Hook', SCRIPT_PARAM_ONOFF, true) end
	print("<font color = '#00FFFF' >Trees AutoLantern Loaded</font>")
end

function Lantern(id)
	p = CLoLPacket(0x3A)
	p.dwArg1 = 1
	p.dwArg2 = 0
	p:EncodeF(myHero.networkID)
	p:EncodeF(id)
	SendPacket(p)
end

function OnCreateObj(obj)
	DelayAction(function(obj)
		if obj and obj.valid and obj.name == 'ThreshLantern' and obj.team ~= TEAM_ENEMY then lant = obj end
	end, 0, {obj})
end

function OnDeleteObj(obj)
	DelayAction(function(obj)
		if obj and obj.valid and obj.name == 'ThreshLantern' and obj.team ~= TEAM_ENEMY then lant = nil end
	end, 0, {obj})
end

function checkHP()
	if Menu.Auto and myHero.health < (myHero.maxHealth * ( Menu.Health)/100) then return true end
	return false
end

function OnTick()
	if Menu.Blitzcrank and myHero:CanUseSpell(_Q) == READY then return end
	if lant and GetDistanceSqr(lant, myHero) <= 90000 and (Menu.Lantern or checkHP()) then Lantern(lant.networkID) end
end

function OnRecvPacket(p)
	if p.header == 0xB4 and Menu.Blitz and lant and GetDistanceSqr(lant, myHero) <= 90000  then
		p.pos = 1
		local thanks = p:DecodeF()
		p.pos = p.size - 25
		local Honda = p:Decode1()
		p.pos = p.size - 44
		local hai = p:Decode1()
		if myHero.networkID == thanks and ( Honda == 0 or Honda == 128 ) and hai ~= 0 then DelayAction(function() Lantern(lant.networkID) end, 0.1) end
	end
end
