require 'VPrediction'
require 'Selector'

if myHero.charName ~= 'Urgot' then return end
local curVersion = '0.05'

local Menu
local VP
local ts
local Target
local Spells = { 
	[0] = { Name = 'Acid Hunter', Range = 1000, Speed = 1600, Delay = 0.10, Width = 100, Color = 0xCC0000},
	[2] = { Name = 'Noxian Corrosive Charge', Range = 900, Speed = 1750, Delay = 0.283, Width = 0, Color = 0x339900},
	[3] = { Name =  'Hyper-Kinetic Position Reverser', Range = 400, Speed = 1800, Delay = 650, Width = 70, Color = 0x0000FF},
}
local Q = Spells.Q
local E = Spells.E
local R = Spells.R

local Ignite
local UrgotEBuff = 'UrgotPlasmaGrenadeBoom'	
local EBuff = {}
local spellTable = { [0] = 'Q', [1] = 'W', [2] = 'E', [3] = 'R' }
local EnemyTable = {}

function OnLoad()
	VP = VPrediction()
	Menu()
	Selector.Instance()
	--Turrets = GetTurrets()
	Ignite = (myHero:GetSpellData(SUMMONER_1).name:find('SummonerDot') and SUMMONER_1) or (myHero:GetSpellData(SUMMONER_2).name:find('SummonerDot') and SUMMONER_2) or nil
	PrintChat("<font color = '#00FFFF' >>> TreesUrgot v" .. curVersion .. ' Loaded')
end

function OnTick()
	if myHero.dead then return end
	
	KS()
	R.Range =  400 + myHero:GetSpellData(_R).level * 150

	Target = GetCustomTarget()
	if not Target then return end
	
	if EBuff[Target.charName] and ValidT(Target, 1200) and myHero:CanUseSpell(_Q) and ( (Menu.Combo.Combo and Menu.Combo.Q) or (Menu.Harass.Harass and Menu.Harass.Q) or Menu.AutoQOnEBuff) then
		if myHero:CanUseSpell(_W) then CastSpell(_W) end -- lol?>
		CastSpell(_Q, Target.x, Target.z)
	end

	if not (Menu.Combo.Combo or Menu.Harass.Harass) then return end
	local mode = Menu.Combo.Combo and 'Combo' or 'Harass'

	if Menu[mode]['Muramana'] then MuramanaOn() end
	
	if Menu[mode]['E'] and myHero:CanUseSpell(_E) and ValidT(Target, E.Range) then
		local CastPosition,  HitChance,  Position = VP:GetCircularCastPosition(Target, E.Delay, E.Width,  E.Range, E.Speed)
		if HitChance >= Menu[mode]['EChance'] and GetVisionDistanceSqr(CastPosition) < E.Range * E.Range then CastSpell(_E, CastPosition.x, CastPosition.z) end
	end
		
	if Menu[mode]['Q'] and myHero:CanUseSpell(_Q) and ValidT(Target, Q.Range) then
		if EBuff[Target.charName] then CastSpell(_Q, Target.x, Target.z)
		else
			local CastPosition,  HitChance,  Position = VP:GetLineCastPosition(Target, Q.Delay, Q.Width,  Q.Range, Q.Speed, myHero, true)
			if HitChance >= Menu[mode]['QChance'] and GetVisionDistanceSqr(CastPosition) < Q.Range * Q.Range then CastSpell(_Q, CastPosition.x, CastPosition.z) end
		end
	end
end

function OnDraw()
	if not Menu.Draw.Enabled or myHero.dead then return end
	for i = 0, 3 do
		if spellTable[i] and Menu.Draw[spellTable[i]] and myHero:CanUseSpell(i) == READY then DrawMyHero(Spells[i]) end
	end
end

function OnGainBuff(unit, buff)
	if unit and unit.valid and unit.type == myHero.type and unit.team == TEAM_ENEMY and buff.name == UrgotEBuff then EBuff[unit.charName] = true return end
end

function OnUpdateBuff(unit, buff)	
	if unit and unit.valid and unit.type == myHero.type and unit.team == TEAM_ENEMY and buff.name == UrgotEBuff then EBuff[unit.charName] = true return end
end

function OnLoseBuff(unit, buff)
	if unit and unit.valid and unit.type == myHero.type and unit.team == TEAM_ENEMY and buff.name == UrgotEBuff then EBuff[unit.charName] = false return end
end

function Menu()
	Menu = scriptConfig('Trees Urgot', 'UrgotTrees')
	Menu:addSubMenu('Combo Settings', 'Combo')
	Menu.Combo:addParam('Q', 'Combo: Use ' .. Q.Name .. ' (Q)', SCRIPT_PARAM_ONOFF, true)
	Menu.Combo:addParam('QChance', 'Q Accuracy', SCRIPT_PARAM_SLICE, 1, 0, 5)
	Menu.Combo:addParam('E', 'Combo: Use ' .. E.Name .. ' (E)', SCRIPT_PARAM_ONOFF, true)
	Menu.Combo:addParam('EChance', 'E Accuracy', SCRIPT_PARAM_SLICE, 1, 0, 5)
	Menu.Combo:addParam('Muramana', 'Toggle Muramana', SCRIPT_PARAM_ONOFF, true)
	Menu.Combo:addParam('Combo', 'Combo Key', SCRIPT_PARAM_ONKEYDOWN, false, 32)

	Menu:addSubMenu('Harass Settings', 'Harass')
	Menu.Harass:addParam('Q', 'Harass: Use '..Q.Name.. ' (Q)', SCRIPT_PARAM_ONOFF, true)
	Menu.Harass:addParam('QChance', 'Q Accuracy', SCRIPT_PARAM_SLICE, 1, 0, 5)
	Menu.Harass:addParam('E', 'Harass: Use '..E.Name.. ' (R)', SCRIPT_PARAM_ONOFF, false)
	Menu.Harass:addParam('EChance', 'Q Accuracy', SCRIPT_PARAM_SLICE, 1, 0, 5)
	Menu.Harass:addParam('Muramana', 'Toggle Muramana', SCRIPT_PARAM_ONOFF, true)
	Menu.Harass:addParam('Harass', 'Harass Key', SCRIPT_PARAM_ONKEYDOWN, false, GetKey('C'))
	
	Menu:addSubMenu('Draw Settings', 'Draw')
	Menu.Draw:addParam('Enabled', 'Enable Draw', SCRIPT_PARAM_ONOFF, false)
	Menu.Draw:addParam('Q', 'Draw ' .. Q.Name .. ' (Q)', SCRIPT_PARAM_ONOFF, true)
	Menu.Draw:addParam('E', 'Draw ' .. E.Name .. ' (E)', SCRIPT_PARAM_ONOFF, false)
	Menu.Draw:addParam('R', 'Draw '.. R.Name .. ' (R)', SCRIPT_PARAM_ONOFF, true)
	
	Menu:addParam('AutoQOnEBuff', 'Auto-Q to Poisoned Enemies', SCRIPT_PARAM_ONOFF, true)
	Menu:addParam('Selector', 'Use Selector', SCRIPT_PARAM_ONOFF, true)
	
	ts = TargetSelector(TARGET_LESS_CAST_PRIORITY, 1200, DAMAGE_PHYSICAL)
	ts.name = 'Urgot'
	Menu:addTS(ts)
end

function DrawMyHero(spell)
	DrawCircle3D(myHero.x, myHero.y, myHero.z, spell.Range, 2, spell.Color)
end

function GetCustomTarget()
	if Menu.Selector then return Selector.GetTarget() end
	if _G.MMA_Target and _G.MMA_Target.type == myHero.type then return _G.MMA_Target end
	if _G.AutoCarry and _G.AutoCarry.Crosshair and _G.AutoCarry.Attack_Crosshair and _G.AutoCarry.Attack_Crosshair.target and _G.AutoCarry.Attack_Crosshair.target.type == myHero.type then return _G.AutoCarry.Attack_Crosshair.target end
	ts:update()
	return ts.target
end

function ValidT(object, distance)
    return object and object.valid and object.type == myHero.type and object.team == TEAM_ENEMY and object.visible and not object.dead and object.bTargetable and object.bInvulnerable == 0 and (distance == nil or GetVisionDistanceSqr(object) <= distance * distance)
end

function GetVisionDistanceSqr(p1, p2)
    if not p2 or not p2.valid then p2 = player end
    if p1 and p1.valid and type(p1.networkID) == 'number' and p1.networkID == p1.networkID and p1.networkID ~= 0 and p1.visionPos then p1 = p1.visionPos end
    if p2 and p2.valid and type(p2.networkID) == 'number' and p2.networkID == p2.networkID and p2.networkID ~= 0 and p2.visionPos then p2 = p2.visionPos end
    return GetDistanceSqr(p1, p2)
end

function KS()
	if myHero:CanUseSpell(_E) == READY then ksE() end 
	if myHero:CanUseSpell(_Q) == READY then ksQ() end
	if Ignite and myHero:CanUseSpell(Ignite) then ksIgnite() end
end

function ksQ()
	for _, Enemy in pairs(EnemyTable) do
		if ValidT(Enemy, 1200) and getDmg('Q', Enemy, myHero) >= Enemy.health then
			if EBuff[Enemy.charName] then CastSpell(_Q, Enemy.visionPos.x, Enemy.visionPos.z) return
			else
				local CastPosition,  HitChance,  Position = VP:GetLineCastPosition(Target, Q.Delay, Q.Width,  Q.Range, Q.Speed, myHero, true)
				if HitChance >= Menu['Combo']['QChance'] and GetVisionDistanceSqr(CastPosition) < Q.Range * Q.Range then CastSpell(_Q, CastPosition.x, CastPosition.z) return end
			end
		end
	end
end

function ksE()
	for _, Enemy in pairs(EnemyTable) do
		if ValidT(Enemy, E.Range) and getDmg('E', Enemy, myHero) >= Enemy.health then 
			local CastPosition,  HitChance,  Position = VP:GetCircularCastPosition(Target, E.Delay, E.Width,  E.Range, E.Speed)
			if HitChance >= Menu['Combo']['EChance'] and GetVisionDistanceSqr(CastPosition) < E.Range * E.Range then CastSpell(_E, CastPosition.x, CastPosition.z) return end
		end
	end
end

function ksIgnite()
	for _, Enemy in pairs(EnemyTable) do
		if ValidT(Enemy, 600) and getDmg('IGNITE', Enemy, myHero) >= Enemy.health then CastSpell(Ignite, Enemy) return end
	end
end