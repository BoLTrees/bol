--[[
	@Name: Pickpocket Alerter
	@Version: 0.01
	@Author: Trees
	@Description: Prints an alert when pickpocket (via spellthief or bandit mastery) comes off cooldown.

]]

local buffNames = {'itempickpocketcd', 'masterypickpocketcd'}
local alert = {'Spellthief', 'Bandit'}

function OnLoad()
	PrintAlert('Pickpocket Alerter Loaded', 2, 0, 255, 255)
end

function OnLoseBuff(unit, buff)
	if unit and unit.valid and unit.isMe then
		for i, v in ipairs(buffNames) do
			if v == buff.name:lower() then PrintAlert( alert[i] .. ' Ready', 2, 0, 255, 255) end
		end
	end
end